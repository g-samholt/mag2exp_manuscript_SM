# Virtual experiments in computational magnetism: _mag2exp_

S. J. R. Holt<sup>1</sup>, M. Lang<sup>1,2</sup>, J. C. Loudon<sup>3</sup>, T. J. Hicken<sup>4</sup>, D. Suess<sup>5,6</sup>, D. Cortés-Ortuño<sup>7</sup>, S. A. Pathak<sup>1</sup>, M. Beg<sup>8</sup>, and H. Fangohr<sup>1,2,9</sup>

<sup>1</sup> *Max Planck Institute for the Structure and Dynamics of Matter, Luruper Chaussee 149, 22761 Hamburg, Germany*  
<sup>2</sup> *Faculty of Engineering and Physical Sciences, University of Southampton, Southampton SO17 1BJ, United Kingdom*  
<sup>3</sup> *Department of Materials Science and Metallurgy, University of Cambridge, Cambridge, CB3 0FS, United Kingdom*  
<sup>4</sup> *Laboratory for Muon Spin Spectroscopy, Paul Scherrer Institute, 5303 Villigen PSI, Switzerland*
<sup>5</sup> *Faculty of Physics, University of Vienna, Boltzmanngasse 5, 1090 Vienna, Austria*  
<sup>6</sup> *Research Platform MMM Mathematics-Magnetism-Materials, University of Vienna, Oskar-Morgenstern-Platz 1, 1090 Vienna, Austria*  
<sup>7</sup> *Paleomagnetic Laboratory Fort Hoofddijk, Utrecht University, Utrecht, 3584 CD, Netherlands*  
<sup>8</sup> *Department of Earth Science and Engineering, Imperial College London, London SW7 2AZ, UK*  
<sup>9</sup> *Center for Free-Electron Laser Science, Luruper Chaussee 149, 22761 Hamburg, Germany*  


| Description | Badge |
| --- | --- |
| Paper | Coming soon ... |
| Preprint | Coming soon ... |
| Binder | [![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fg-samholt%2Fmag2exp_manuscript_SM.git/HEAD) |
| License | [![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) |
| DOI | Coming soon … |


## About

This repository provides Ubermag simulation code to reproduce results from
the mag2exp manuscript. All notebooks hosted in this repository can be run in the cloud (see
next section on Binder), and thus the results reproduced by anybody.


## Binder

Jupyter notebooks hosted in this repository can be executed and modified in the
cloud via Binder. This does not require you to have anything installed and no
files will be created on your machine. To access Binder, click on the Binder
badge in the table above. For faster execution we recommend a local installation.

## Install Locally with conda

To get started make sure you have conda installed and then clone the repository:
```bash
git clone https://gitlab.mpcdf.mpg.de/g-samholt/mag2exp_manuscript_SM.git
```
Change into that directory:
```bash
cd mag2exp_manuscript_SM
```
Create a new `conda` environment, here called `mag2exp_SM`, and install the packages needed:

```bash
conda env create -f environment.yaml
conda activate mag2exp_SM
```
You're all ready to go!

## License

Licensed under the BSD 3-Clause "New" or "Revised" License. For details, please
refer to the [LICENSE](LICENSE) file.

## How to cite


## Acknowlegdements

- EPSRC Programme Grant on [Skyrmionics](http://www.skyrmions.ac.uk) (EP/N032128/1)
